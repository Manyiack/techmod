package com.tech.techmod.registry;

import com.tech.techmod.TechMod;
import com.tech.techmod.blocks.BaseResources;

import abused_master.abusedlib.registry.RegistryHelper;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.util.registry.Registry;

public class ModBlocks
{





    public static void registerBlocks()
    {
        
        for(BaseResources.EnumOres ore : BaseResources.EnumOres.values())
        {
           // RegistryHelper.registerBlock(TechMod.MODID, ore.getBlockOres());
           Registry.register(Registry.BLOCK, ore.getBlockOres().getNameIdentifier(TechMod.MODID), ore.getBlockOres());
           Registry.register(Registry.ITEM, ore.getBlockOres().getNameIdentifier(TechMod.MODID), new BlockItem(ore.getBlockOres(), new Item.Settings().group(ore.getBlockOres().getTab())));
        }

        for(BaseResources.EnumOreBlock ore : BaseResources.EnumOreBlock.values())
        {
           // RegistryHelper.registerBlock(TechMod.MODID, ore.getBlockOres());
           Registry.register(Registry.BLOCK, ore.getBlockOres().getNameIdentifier(TechMod.MODID), ore.getBlockOres());
           Registry.register(Registry.ITEM, ore.getBlockOres().getNameIdentifier(TechMod.MODID), new BlockItem(ore.getBlockOres(), new Item.Settings().group(ore.getBlockOres().getTab())));
        }
        

    }
}