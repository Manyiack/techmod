package com.tech.techmod.registry;

import com.tech.techmod.blocks.BaseResources;

import abused_master.abusedlib.registry.RegistryHelper;

public class WorldGenReg
{
    public static void addOreFeatures()
    {
        for(BaseResources.EnumOres ores : BaseResources.EnumOres.values())
        {
            if(ores.generateOre())
            {
                RegistryHelper.generateOreInStone(ores.getBlockOres(), ores.getVeinSize(), ores.getSpawnRate(), ores.getMaxHeight());
            }
        }
    }
}