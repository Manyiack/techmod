package com.tech.techmod.blocks;

import java.util.Locale;

import com.tech.techmod.TechMod;
import com.tech.techmod.util.OreGenConfig;
import com.tech.techmod.util.OreGenEntry;

import abused_master.abusedlib.blocks.BlockBase;
import net.minecraft.block.Material;

public class BaseResources extends BlockBase
{
    public BaseResources(String name, float hardness)
    {
        super(name, Material.STONE, hardness, TechMod.modItemGroup);
    }

    public enum EnumOres
    {
        COPPER_ORE(1.0f, OreGenConfig.INSTANCE.getEntry("copper_ore")),
        TIN_ORE(1.0f, OreGenConfig.INSTANCE.getEntry("tin_ore")),
        SILVER_ORE(3.0f, OreGenConfig.INSTANCE.getEntry("silver_ore")),
        LEAD_ORE(2.0f, OreGenConfig.INSTANCE.getEntry("lead_ore"));

        private BaseResources blockOres;
        private int maxHeight;
        private int spawnRate;
        private boolean generateOre;
        private int veinSize;

        EnumOres(float hardness, OreGenEntry entry)
        {
            this.blockOres = new BaseResources(getName(), hardness);
            this.maxHeight = entry.getMaxHeight();
            this.spawnRate = entry.getCount();
            this.generateOre = entry.doesGenerate();
            this.veinSize = entry.getSize();
        }

        public String getName()
        {
            return this.toString().toLowerCase(Locale.ROOT);
        }

        public BaseResources getBlockOres()
        {
            return this.blockOres;
        }

        public int getMaxHeight()
        {
            return this.maxHeight;
        }

        public int getSpawnRate()
        {
            return this.spawnRate;
        }

        public int getVeinSize()
        {
            return this.veinSize;
        }

        public boolean generateOre()
        {
            return this.generateOre;
        }
    }

    public enum EnumOreBlock
    {
        COPPER_BLOCK,
        TIN_BLOCK,
        SILVER_BLOCK,
        LEAD_BLOCK;

        private BaseResources blockOres;

        EnumOreBlock()
        {
            this.blockOres = new BaseResources(getName(), 2.0f);
        }

        public String getName()
        {
            return this.toString().toLowerCase(Locale.ROOT);
        }

        public BaseResources getBlockOres()
        {
            return this.blockOres;
        }
    }
}