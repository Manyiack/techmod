package com.tech.techmod;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.tech.techmod.registry.ModBlocks;
import com.tech.techmod.registry.WorldGenReg;
import com.tech.techmod.util.OreGenConfig;

import abused_master.abusedlib.utils.Config;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.block.Blocks;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;

public class TechMod implements ModInitializer
{
    public static final String RESOURCES_TAG = "c";
    public static String MODID = "techmod";

    public static Config config;
    public static File rmfolder = new File(FabricLoader.getInstance().getConfigDirectory().getPath() + "/techmod");

    public static ItemGroup modItemGroup = FabricItemGroupBuilder.build(new Identifier(MODID, "techmod"), () -> new ItemStack(Blocks.STONE));




    @Override
    public void onInitialize()
    {
        config = new Config("techmod/config", this.loadConfig());
        OreGenConfig.INSTANCE.initOreGen();
        ModBlocks.registerBlocks();
        WorldGenReg.addOreFeatures();
    }

    public Map<String, Object> loadConfig()
    {
        Map<String, Object> configOptions = new HashMap<>();
        configOptions.put("coalGen_Generation", 10);

        return configOptions;
    }
}